/**
 * Module dependencies.
 */
var express    = require("express");
var user = require('./routes/users');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var expressValidator = require('express-validator');

//middleware
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var router = express.Router();

router.get('/', function(req, res) {
    res.json({ message: 'Welcome Node' });
});

router.post('/register',user.register);
router.post('/login',user.login);
router.get('/user',user.user);
router.post('/create_user',user.create_user);
router.get('/get_user/:id', user.get_user);
router.post('/update_user/:id',user.update_user);
router.get('/delete_user/:id', user.delete_user);
router.post('/change_status',user.change_status);
router.post('/fpass',user.fpass);
router.post('/cotp',user.cotp); 
router.post('/changepassword',user.changepassword); 
router.post('/socialsignup',user.socialsignup);
router.post('/sociallogin',user.sociallogin); 
router.post('/site_setting',user.site_setting);
router.get('/get_site_setting/:id',user.get_site_setting)

app.use('/', router);
app.listen(4000);

module.exports = router;
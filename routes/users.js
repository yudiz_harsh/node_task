var express = require('express');
var app = express();
var mysql = require('mysql');
var bcrypt = require('bcrypt');
var random = require("random-js")();
var nodemailer = require('nodemailer');

// Connection With Database
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 8889,
  database: 'node_task'
});
connection.connect(function (err) {
  if (!err) {
    console.log("Done 6 bhai....");
  } else {
    console.log("Error 6 bhai....");
  }
});

// Registration of User
exports.register = function (req, res) {
  var today = new Date();
  bcrypt.hash(req.body.password, 5, function (err, bcryptedPassword) {

    var users = {
      "firstname": req.body.firstname,
      "lastname": req.body.lastname,
      "email": req.body.email,
      "password": bcryptedPassword,
      "created": today,
      "modified": today
    }

    req.checkBody('firstname', 'First name required').notEmpty();
    req.checkBody('lastname', 'Last name required').notEmpty();
    req.checkBody('password', 'Password required').notEmpty();
    req.checkBody('password', '6 to 20 characters required').len(6, 20);
    req.checkBody('email', 'Email required').notEmpty();
    req.checkBody('email', 'Invalid Email').isEmail();
    var errors = req.validationErrors();

    if (errors) {
      res.send({
        "code": 200,
        data: errors
      });
    }
    else{
      connection.query('INSERT INTO tbl_user SET ?', users, function (error, results, fields) {

      if (errors) {
        res.json({
          status: 412,
          data: errors
        });
      } else {
        res.send({
          "code": 200,
          "success": "user registered thyo"
        });
      }
    });
    }
  });
}

// Login User
exports.login = function (req, res) {
  var email = req.body.email;
  var password = req.body.password;
  connection.query('SELECT * FROM tbl_user WHERE email = ?', [email], function (error, results, fields) {
    if (error) {
      res.send({
        "code": 400,
        "failed": "kaik khotu thyu"
      })
    } else {
      if (results.length > 0) {
        bcrypt.compare(password, results[0].password, function (err, doesMatch) {
          if (doesMatch) {
            res.send({
              "code": 200,
              "success": "login thyo"
            });
          } else {
            res.send({
              "code": 204,
              "success": "Email and password nhi malta"
            });
          }
        });
      } else {
        res.send({
          "code": 204,
          "success": "Email 6 j nhi"
        });
      }
    }
  });
}



/*
 * GET User listing.
 */
exports.user = function (req, res) {
  var data = {
    "error": 1,
    "users": ""
  };

  connection.query("SELECT * from tbl_user", function (err, rows, fields) {
    if (rows.length != 0) {
      data["error"] = 0;
      data["users"] = rows;
      res.json(data);
    } else {
      data["users"] = 'users na malya..';
      res.json(data);
    }
  });
};


// Create New User

exports.create_user = function (req, res) {

  var today = new Date();
  var users = {
    "firstname": req.body.firstname,
    "lastname": req.body.lastname,
    "email": req.body.email,
    "created": today,
    "modified": today
  }

  req.checkBody('firstname', 'First name required').notEmpty();
  req.checkBody('lastname', 'Last name required').notEmpty();
  req.checkBody('email', 'Email required').notEmpty();
  req.checkBody('email', 'Invalid Email').isEmail();
  var errors = req.validationErrors();

   if (errors) {
      res.send({
        "code": 200,
        data: errors
      });
    }
  else{
    connection.query('INSERT INTO tbl_user SET ?', users, function (error, results, fields) {
    if (errors) {
      res.json({
        status: 412,
        data: errors
      });
    } else {
      res.send({
        "code": 200,
        "success": "user add thyo"
      });
  }
    
  
  });
  };
};



// Get User By Id

exports.get_user = function (req, res) {

  var id = req.params.id;
  console.log(' id ' + id);
  var data = {
    "error": 1,
    "users": ""
  };
  connection.query('SELECT * FROM tbl_user WHERE uid = ? ', [id], function (err, rows, fields) {
    console.log(rows)
    if (rows.length != 0) {
      data["error"] = 0;
      data["users"] = rows;
      res.json(data);
    } else {
      data["users"] = 'users na malya..';
      res.json(data);
    }
  });
};



// Update user

exports.update_user = function (req, res) {
  var id = req.params.id;
  var firstname = req.body.firstname;
  var lastname = req.body.lastname;
  var email = req.body.email;
  var data = {
    "error": 1,
    "user": ""
  };

  if (id && firstname && lastname && email) {
    connection.query("UPDATE tbl_user SET firstname=?, lastname=?, email=? WHERE uid=?", [firstname, lastname, email, id], function (err, rows, fields) {
      if (err) {
        data["user"] = "kaik khotu thyu";
      } else {
        data["error"] = 0;
        data["user"] = "user update thyo";
      }
      res.json(data);
    });
  } else {
    data["user"] = "bdhu j joy e (i.e : id, firstname, lastname, email)";
    res.json(data);
  }
};




// Delete User

exports.delete_user = function (req, res) {
  var id = req.params.id;
  console.log(' id ' + id);
  var data = {
    "error": 1,
    "users": ""
  };

  connection.query('DELETE FROM tbl_user WHERE uid = ? ', [id], function (err, rows, fields) {
    //console.log(rows)
    var numRows = rows.affectedRows;
    if (numRows > 0) {
      if (rows.length != 0) {
        data["error"] = 0;
        data["users"] = 'user gyo';
        res.json(data);
      }
    } else {
      data["users"] = 'users na malya..';
      res.json(data);
    }
  });
};



// Change Status

exports.change_status = function (req, res) {

  var uid = req.body.uid;
  var status = req.body.status;
  var data = {
    "error": 1,
    "message": ""
  };
  if (uid && status) {
    connection.query("UPDATE tbl_user SET status=? WHERE uid=?", [status, uid], function (err, result, fields) {

      if (err) {
        data["message"] = "kaik khotu thyu";
      } else {
        var numRows = result.affectedRows;
        if (numRows > 0) {
          if (status == 0) {
            data["error"] = 0;
            data["message"] = "user inactive thyo";
          } else {
            data["error"] = 0;
            data["message"] = "user active thyo";
          }
        } else {
          data["message"] = "user nthi mlto";
        }
      }
      res.json(data);
    });
  } else {
    data["message"] = "bdhu j joy e (i.e : id , Status)";
    res.json(data);
  }
};



// Forgot password

exports.fpass = function (req, res) {
  var email = req.body.email;
  var data = {
    "error": 1,
    "message": ""
  };

  connection.query('SELECT * FROM tbl_user WHERE email = ? ', [email], function (err, rows, fields) {
    console.log(rows[0])
    if (rows.length != 0) {
      var value = random.integer(222222, 888888);
      var transporter = nodemailer.createTransport({
        host: 'smtp.googlemail.com',
        port: 465,
        secure: true,
        auth: {
          user: 'harsh@ping2world.com',
          pass: 'harsh017'
        }
      });

      var mailOptions = {
        from: 'harsh@ping2world.com',
        to: email,
        subject: 'One time password',
        html: 'ahiya tamro ek time no password <b>' + value + '</b>'
      };
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        } else {
          connection.query("UPDATE tbl_user SET OTP=? WHERE uid=?", [value, rows[0].uid], function (err, rows, fields) {
            data["message"] = "ek time no password moklyo tamara mail ma jovo";
            res.json(data)
          });
        }
      });
    } else {
      data["users"] = 'users na malya...';
      res.json(data);
    }
  });
};



// Send OTP After Compare OTP And enter New Password

exports.cotp = function (req, res) {

  var email = req.body.email;
  var OTP = req.body.OTP;
  var password = req.body.password;

  var data = {
    "error": 1,
    "message": ""
  };
  req.checkBody('email', 'Email required').notEmpty();
  req.checkBody('email', 'Invalid Email').isEmail();
  req.checkBody('email', 'Email required').notEmpty();
  req.checkBody('password', 'Password requires').notEmpty();

  var errors = req.validationErrors();

  bcrypt.hash(req.body.password, 5, function (err, bcryptedPassword) {

    var users = {
      "password": bcryptedPassword
    }

    connection.query('SELECT * FROM tbl_user WHERE email = ? and OTP =? ', [email, OTP], function (err, rows, fields) {
      console.log(rows[0])
      if (rows.length != 0) {

        connection.query("UPDATE tbl_user SET password=? WHERE uid=?", [users.password, rows[0].uid], function (err, rows, fields) {
          if (errors) {
            res.json({status: 412,data: errors});
          } else {
            data["message"] = "password badlayo";
            res.json(data);
          }

        });

      } else {
        data["message"] = 'khoto otp ka email';
        res.json(data);
      }
    });
  });
};



// Change Password

exports.changepassword = function (req, res) {
  var uid = req.body.uid;
  var oldpassword = req.body.oldpassword;
  var newpassword = req.body.newpassword;
  var data = {
    "error": 1,
    "message": ""
  };
  
  bcrypt.hash(req.body.oldpassword, 5, function (err, bcryptedPassword) {

    var users = {
      "password": bcryptedPassword
    }

    connection.query('SELECT * FROM tbl_user WHERE uid = ? ', [uid], function (err, rows, fields) {
      console.log(rows[0])

      if (rows.length != 0) {
        bcrypt.compare(oldpassword, rows[0].password, function (err, doesMatch) {
          if (doesMatch) {
            bcrypt.hash(req.body.newpassword, 5, function (err, bcryptedPassword) {

              var pass = {
                "password": bcryptedPassword
              }
              connection.query("UPDATE tbl_user SET password=? WHERE uid=?", [pass.password, rows[0].uid], function (errors, rows, fields) {
                if (errors) {
                  res.json({status: 412,data: errors});
                } else {
                  data["message"] = "password badlayo";
                  res.json(data);
                }
              });
            });
          } else {
            res.send({"code": 204,"success": "password no med nthi avto"});
          }
        });
      }
    });
  });
};


// Social SignUp
exports.socialsignup = function (req, res) {

  var today = new Date();

  var users = {
    "firstname": req.body.firstname,
    "lastname": req.body.lastname,
    "email": req.body.email,
    "social_id": req.body.social_id,
    "social_provider": req.body.social_provider,
    "created": today,
    "modified": today
  }
  req.checkBody('firstname', 'First name required').notEmpty();
  req.checkBody('lastname', 'Last name required').notEmpty();
  req.checkBody('email', 'Email required').notEmpty();
  req.checkBody('email', 'Invalid Email').isEmail();
  req.checkBody('social_id', 'Social-Id requires').notEmpty();
  req.checkBody('social_provider', 'Social-Provider requires').notEmpty();
  var errors = req.validationErrors();

  if (errors) {
      res.send({"code": 200,data: errors});
    }
  else{
    connection.query('INSERT INTO tbl_user SET ?', users, function (error, results, fields) {
    if (errors) {
      res.json({status: 412,data: errors});
    } else {
      res.send({"code": 200,"success": "user register thyo"});
    }
  });
  }
};



// Social Login 

exports.sociallogin = function (req, res) {
  var email = req.body.email;
  var social_id = req.body.social_id;
  connection.query('SELECT * FROM tbl_user WHERE email = ? and social_id = ? ', [email, social_id], function (error, results, fields) {
    if (error) {
        res.json({status: 412,data: errors});
    } else {
        if (results.length > 0) {
          res.send({"code": 200,"success": "login thyo"});
        } else {
          res.send({"code": 204,"success": "email 6 j nhi"});
        }
    }
  });
}



// Site Setting
exports.site_setting = function (req, res) {
  var today = new Date();
  var users = {
    "name": req.body.name,
    "value": req.body.value,
  }
  req.checkBody('name', ' Name required').notEmpty();
  req.checkBody('value', 'Value required').notEmpty();
  var errors = req.validationErrors();

  connection.query('INSERT INTO site_settings SET ?', users, function (error, results, fields) {
    if (errors) {
      res.json({status: 412,data: errors});
    } else {
      res.send({"code": 200,"success": "setting save thay gya"});
    }
  });
};



// get site setting 
exports.get_site_setting = function (req, res) {
  var data = {"error": 1,"message": ""};
  var id = req.params.id;
  console.log(' id ' + id);
  var data = {"error": 1,"users": ""};

  connection.query('SELECT * FROM site_settings WHERE id = ? ', [id], function (err, rows, fields) {
    if (rows.length != 0) {
      data["error"] = 0;
      data["users"] = rows;
      res.json(data);
    } else {
      data["users"] = 'setting na mlyu';
      res.json(data);
    }
  });
}